#!/usr/bin/env python3
# -*- encoding: utf-8; py-indent-offset: 4 -*-
#
#  ____  _          _ _       
# / ___|| |__   ___| | |_   _ 
# \___ \| '_ \ / _ \ | | | | |
#  ___) | | | |  __/ | | |_| |
# |____/|_| |_|\___|_|_|\__, |
#                      |___/ 
#
# created: 01/2022
# last Update 2023-09-09
#
# Author: Frank Baier
# E-Mail: dev@baier-nt.de
#
from cmk.gui.i18n import _
from cmk.gui.plugins.metrics.utils import metric_info

metric_info['shelly_energy_returned'] = {
    "title": _("Returned electrical energy"),
    "unit": "wh",
    "color": "#aa80b0",
}

metric_info['shelly_pf'] = {
    "title": _("Power factor"),
    "unit": "",
    "color": "#aa00b0",
}

