# Shelly Checkmk addon

This project contains an addon for Shelly devices (https://shelly.cloud/).

The special agent supports all Shelly devices and adds additional information about energy, power, current on EM and 3EM devices.

Don't hesitate to ask for additional check plugins.

# Bugs / Important stuff

- no bugs known at the moment.

# Building

To build all the plugins, clone [this repository]
and run:

    make all

(Make sure to have make and Python3 installed.)

You can also build a single plugin, e.g.:

    make 

The files (*.mkp) will be written into the `build/` directory.


# Installing

SSH into your Check_MK machine, change to the user of the site you want to install the plugin,
then run:

    mkp install /path/to/plugin.mkp

or use WATO to install.

then configure the scpecial agent for shelly device


# License
Apache 2
